# Web Image Converter

Example app using rust to wasm and react.

## Build WASM

Change to wasm folder and install wasm-pack.

```bash
cd wasm
cargo install wasm-pack
wasm-pack build --release
```

## Build react app

```bash
cd react-client
npm install
```

## Run

```bash
cd react-client
npm run start
```

## Source

Based on [https://medium.com/swlh/intro-to-webassembly-in-react-with-rust-d067408231b9](https://medium.com/swlh/intro-to-webassembly-in-react-with-rust-d067408231b9)
