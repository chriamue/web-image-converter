import React, { useRef, useState } from 'react';
import Webcam from "react-webcam";
import './App.css';

interface Props {
}

const videoConstraints = {
  width: 1280,
  height: 720,
  facingMode: "user"
};

const App: React.FC<Props> = () => {

  const [dataUri, setDataUri] = useState<any>('');
  const webcamRef = useRef<Webcam>(null);

  const dataURLtoBlob = (dataUrl: string, callback: Function) => {
    var req = new XMLHttpRequest();

    req.open('GET', dataUrl);
    req.responseType = 'blob';

    req.onload = function fileLoaded(e) {
      callback(this.response);
    };

    req.send();
  }


  const capture = React.useCallback(() => {
    const imageSrc = webcamRef ? webcamRef.current!.getScreenshot() : null;
    if (imageSrc) {
      dataURLtoBlob(imageSrc, (data: Blob) => {
        fileToBlob(data)
          .then((data) => {
            if (data instanceof ArrayBuffer) {
              const uint8Arr = new Uint8Array(data);
              import('wasm').then(({ processImageCanny }) => {
                let content = processImageCanny(uint8Arr);
                blobToDataUri(new Blob([content.buffer], { type: 'image/png' } /* (1) */))
                  .then(dataUri => {
                    setDataUri(dataUri);
                  })
              });
            }
          })
      });
    }
  }, [webcamRef, setDataUri]);

  const blobToDataUri = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      if (event == null) {
        resolve("");
      } else {
        resolve(event.target.result)
      }
    };
    reader.readAsDataURL(blob);
  })

  const fileToBlob = (file: File | Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      if (event == null) {
        resolve("");
      } else {
        resolve(event.target.result)
      }
    };
    reader.readAsArrayBuffer(file);
  })

  const onChange = (file: File | Blob) => {

    if (!file) {
      setDataUri('');
      return;
    }

    fileToBlob(file)
      .then((data) => {
        if (data instanceof ArrayBuffer) {
          const uint8Arr = new Uint8Array(data);
          import('wasm').then(({ processImageCanny }) => {
            let content = processImageCanny(uint8Arr);
            console.log(content);
            blobToDataUri(new Blob([content.buffer], { type: 'image/png' } /* (1) */))
              .then(dataUri => {
                setDataUri(dataUri);
              })
          });
        }
      })
  }

  return (
    <div className="App" >
      <div>
        <img width="200" height="200" src={dataUri} alt="converted" />
        <input type="file" onChange={(event) => event.target.files ? onChange(event.target.files[0] || null) : null} />
      </div>
      <div>
        <Webcam
          audio={false}
          height={480}
          ref={webcamRef}
          screenshotFormat="image/png"
          width={640}
          videoConstraints={videoConstraints}
        />
        <button onClick={capture}>Capture photo</button>
      </div>
    </div>
  );
};

export default App;