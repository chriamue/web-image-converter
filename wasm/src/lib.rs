use wasm_bindgen::prelude::*;
use std::panic;
extern crate console_error_panic_hook;
use std::io::{Cursor, Read, Seek, SeekFrom};

extern crate image;
use image::{DynamicImage};
use imageproc::edges::canny;

#[wasm_bindgen(js_name = processImage)]
pub fn process_image(data: &[u8]) -> Vec<u8> {
   let mut img = image::load_from_memory_with_format(&data, image::ImageFormat::Png).unwrap();
   img.invert();
   let mut c = Cursor::new(Vec::new());
   match img.write_to(&mut c, image::ImageOutputFormat::Png) {
      Ok(_) => (),
      Err(_) => return data.to_vec()
   };
   c.seek(SeekFrom::Start(0)).unwrap();
   let mut out_data = Vec::new();
   c.read_to_end(&mut out_data).unwrap();
   out_data
}

#[wasm_bindgen(js_name = processImageCanny)]
pub fn process_image_canny(data: &[u8]) -> Vec<u8> {
   panic::set_hook(Box::new(console_error_panic_hook::hook));
   let mut img = image::load_from_memory_with_format(&data, image::ImageFormat::Png).unwrap();
   img.invert();
   let img = img.to_luma8();
   let edges = DynamicImage::ImageLuma8(canny(&img, 50.0, 100.0));
   let mut c = Cursor::new(Vec::new());
   match edges.write_to(&mut c, image::ImageOutputFormat::Png) {
      Ok(_) => (),
      Err(_) => return data.to_vec()
   };
   c.seek(SeekFrom::Start(0)).unwrap();
   let mut out_data = Vec::new();
   c.read_to_end(&mut out_data).unwrap();
   out_data
}


#[cfg(test)]
mod tests {
   use std::io::{Cursor, Read, Seek, SeekFrom};
   use image;
   use super::*;

   #[test]
   fn process_image_test() {
      let img = image::open("lena.png").unwrap();
      let mut c = Cursor::new(Vec::new());
      img.write_to(&mut c, image::ImageOutputFormat::Png);
      c.seek(SeekFrom::Start(0)).unwrap();
      let mut out_data = Vec::new();
      c.read_to_end(&mut out_data).unwrap();
      let out_img = process_image_canny(&out_data);
      print!("{}", out_img.len());
      let img = image::load_from_memory_with_format(&out_img, image::ImageFormat::Png).unwrap();
      img.save("lena_test_out.png").unwrap();
   }
}